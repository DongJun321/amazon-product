import time
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException

from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.keys import Keys
import selenium.webdriver.chrome.options
import os
import csv

url = "https://www.amazon.com/ref=nav_logo"
chrome_path=r''+os.getcwd()+'\\driver\\chromedriver.exe'
driver = webdriver.Chrome(chrome_path)
driver.maximize_window()
driver.get(url)

try:
    search_box = EC.presence_of_element_located((By.ID,"twotabsearchtextbox"))
    WebDriverWait(driver, 20).until(search_box)
    time.sleep(1)
    keyword=driver.find_element_by_id("twotabsearchtextbox")
    driver.find_element_by_id("twotabsearchtextbox").clear()
    keyword.send_keys('computer')
    time.sleep(2)
    keyword.send_keys(Keys.ENTER)
except TimeoutException:
    print('no data')
try:
    computer=EC.presence_of_element_located((By.XPATH,'//*[@id="search"]/div[1]/div[2]/div/span[3]/div[1]/div[6]/div/div/div/div[2]/div[2]/div/div[1]/div/div/div[1]/h2/a/span'))    
    WebDriverWait(driver, 20).until(computer)
    time.sleep(1)
    driver.find_element_by_xpath('//*[@id="search"]/div[1]/div[2]/div/span[3]/div[1]/div[6]/div/div/div/div[2]/div[2]/div/div[1]/div/div/div[1]/h2/a/span').click()  
    for i in range(5,11):
        try:
            print(i)
            driver.find_element_by_xpath('//*[@id="altImages"]/ul/li['+str(i)+']').click()
            # time.sleep(5)
            img=driver.find_element_by_xpath('//*[@id="main-image-container"]/ul/li['+str(i)+']/span/span/div/img')
            print(img.get_attribute('src'))
        except NoSuchElementException as e:
            em='no element'
            print(em)
            pass
except TimeoutException:
    print('no element')    
print('Successfull finished. please check "result.csv"')

