import time
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException

from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.keys import Keys
import selenium.webdriver.chrome.options
import os
import csv

url = "https://www.amazon.com/ref=nav_logo"
chrome_path=r''+os.getcwd()+'\\driver\\chromedriver.exe'
driver = webdriver.Chrome(chrome_path)
driver.maximize_window()
driver.get(url)
# language select part
driver.find_element_by_id('icp-nav-flyout').click()
try:
    select_spain=EC.presence_of_element_located((By.XPATH,'//*[@id="customer-preferences"]/div/div/form/div[1]/div[1]/div[2]/div/label/span'))
    WebDriverWait(driver, 20).until(select_spain)
    time.sleep(1)
    driver.find_element_by_xpath('//*[@id="customer-preferences"]/div/div/form/div[1]/div[1]/div[2]/div/label/span').click()
    time.sleep(2)
except TimeoutException:
    print('no find')
driver.find_element_by_xpath('//*[@id="icp-btn-save"]/span/input').click()
# end language select

# search keky word reading from csv file.
with open('keyword.csv') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    next(csv_reader)
    for csv_row in csv_reader:
        each_keyword = csv_row[0]
        print(each_keyword)
    # exit()        
        try:
            search_box = EC.presence_of_element_located((By.ID,"twotabsearchtextbox"))
            WebDriverWait(driver, 20).until(search_box)
            time.sleep(1)
            keyword=driver.find_element_by_id("twotabsearchtextbox")
            driver.find_element_by_id("twotabsearchtextbox").clear()
            keyword.send_keys(each_keyword)
            time.sleep(2)
            keyword.send_keys(Keys.ENTER)
        except TimeoutException:
            print('no data')
        if not os.path.exists(os.getcwd()+'\\result\\'):
            result=os.mkdir(os.getcwd()+'\\result\\')
        # result csv header file making
        with open(os.getcwd()+'\\result\\'+each_keyword+'.csv', 'w',encoding="utf-8", newline='') as fout:
            out = csv.writer(fout, delimiter=",", quoting=csv.QUOTE_ALL)
            out.writerow(['image','title','price','description'])
        # end result csv header file    
        # getting row informations
            # temp to save urls
            product_links=[] 
            for next_page in range(2,20):
                try:
                    # driver.find_element_by_xpath('//*[@id="search"]/div[1]/div[2]/div/span[7]/div/div/div/ul/li['+str(next_page)+']/a').click()
                    driver.find_element_by_xpath('//*[@id="search"]/div[1]/div[2]/div/span[7]/div/div/div/ul/li['+str(next_page)+']').click()
                    time.sleep(9)
                except NoSuchElementException as e:
                    break
                for index in range(1,50):
                    try:
                        # informations=driver.find_element_by_xpath('//*[@id="search"]/div[1]/div[2]/div/span[3]/div[1]/div['+str(index)+']/div/div/div/div[2]')
                        # image=driver.find_element_by_xpath('//*[@id="search"]/div[1]/div[2]/div/span[3]/div[1]/div['+str(index)+']/div/div/div/div[2]/div[1]/div/div/span/a/div/img')
                        product_url=driver.find_element_by_xpath('//*[@id="search"]/div[1]/div[2]/div/span[3]/div[1]/div['+str(index)+']/div/div/div/div[2]/div[2]/div/div[1]/div/div/div[1]/h2/a')
                        # price=driver.find_element_by_xpath('//*[@id="search"]/div[1]/div[2]/div/span[3]/div[1]/div['+str(index)+']/div/div/div/div[2]/div[2]/div/div[2]/div[1]/div/div[1]/div/div/a/span[1]/span[2]')        
                        shipping=driver.find_element_by_xpath('//*[@id="search"]/div[1]/div[2]/div/span[3]/div[1]/div['+str(index)+']/div/div/div/div[2]/div[2]/div/div[2]/div[1]/div/div[2]/div/span')

                        if(str(shipping.text)=='Elegible para enviar a México'):
                            # print(image.get_attribute('src'))
                            print('-----------URL---------------------')
                            print(product_url.get_attribute('href'))
                            # print('----------price---------')
                            # print(price.text)
                            print('-------------shiping-------------')
                            print(shipping.text)
                            print(index)
                            product_links.append(product_url.get_attribute('href'))
                            # out.writerow([image.get_attribute('src'),description.text,price.text])
                    except NoSuchElementException as e:
                        print('Skip to next')
                        try:
                            # image=driver.find_element_by_xpath('//*[@id="search"]/div[1]/div[2]/div/span[3]/div[1]/div['+str(index)+']/div/div/div/div[2]/div[1]/div/div/span/a/div/img')
                            product_url=driver.find_element_by_xpath('//*[@id="search"]/div[1]/div[2]/div/span[3]/div[1]/div['+str(index)+']/div/div/div/div[2]/div[2]/div/div[1]/h2/a')
                            # price=driver.find_element_by_xpath('//*[@id="search"]/div[1]/div[2]/div/span[3]/div[1]/div['+str(index)+']/div/div/div/div[2]/div[3]/div/div[1]/div/div/a/span[1]/span[2]')
                            shipping=driver.find_element_by_xpath('//*[@id="search"]/div[1]/div[2]/div/span[3]/div[1]/div['+str(index)+']/div/div/div/div[2]/div[3]/div/div[2]/div/span')
                            if(str(shipping.text)=='Elegible para enviar a México'):
                                # print(image.get_attribute('src'))
                                print('-----------URL---------------------')
                                print(product_url.get_attribute('href'))
                                # print('----------price---------')
                                # print(price.text)
                                print('-------------shiping-------------')
                                print(shipping.text)
                                print(index)
                                product_links.append(product_url.get_attribute('href'))
                                # out.writerow([image.get_attribute('src'),description.text,price.text])
                        except NoSuchElementException as e:
                            pass
                    print("################################################################")    
            for i in range(len(product_links)-1):
                print(product_links[i])
                print('------------------------------------')
                driver.get(product_links[i])
                try:
                    element_present = EC.presence_of_element_located((By.XPATH,'//*[@id="productTitle"]'))
                    WebDriverWait(driver, 20).until(element_present)
                    time.sleep(1)
                    title=driver.find_element_by_xpath('//*[@id="productTitle"]')
                except TimeoutException:
                    print('no data')
                try:
                    price=driver.find_element_by_xpath('//*[@id="priceblock_ourprice"]')        
                    description=driver.find_element_by_xpath('//*[@id="featurebullets_feature_div"]')
                    image=driver.find_element_by_xpath('//*[@id="landingImage"]')
                    print('---------title-------')
                    print(title.text)            
                    print('---------price-------')
                    print(price.text)
                    print('---------description-------')
                    print(description.text)
                    print('---------vertical image-------')
                    print(image.get_attribute('src'))
                    out.writerow([image.get_attribute('src'),title.text,price.text,description.text])
                except NoSuchElementException as e:
                    pass
print('Successfull finished. please check "result folder"')

